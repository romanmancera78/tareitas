import { AgregarproyectosComponent } from './pages/agregarproyectos/agregarproyectos.component';
import { LoginComponent } from './components/login/login.component';
import { ComponenteComponent } from './components/componente/componente.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { ProyectosComponent } from './pages/proyectos/proyectos.component';
import { PerfilComponent } from './pages/perfil/perfil.component';
import { ProyectoComponent } from './pages/proyecto/proyecto.component';
import { PrincipalComponent } from './pages/principal/principal.component';
import { PublicacionesComponent } from './pages/publicaciones/publicaciones.component';
import { Publicacion1Component } from './pages/publicacion1/publicacion1.component';
import { RegistroComponent } from './pages/registro/registro.component';



const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent,
  },
  {
    path: 'register',
    component: RegistroComponent,
  },
  {
    path: 'l',
    component: HomeComponent,
  },
  {
    path: 'home',
    component: PrincipalComponent,
  },
  {
    path: 'proyectos',
    component: ProyectosComponent,
  },
  {
    path: 'proyecto/:id',
    component: ProyectoComponent,
  },
  {
    path: 'perfil',
    component: PerfilComponent,
  },
  {
    path: 'publicaciones',
    component: PublicacionesComponent,
  },
  {
    path: 'publicacion1',
    component: Publicacion1Component,
  },
  
  {
    path: 'componente',
    component: ComponenteComponent,
  },

  { path: 'agregarproyectos', component: AgregarproyectosComponent}
  ,

  { path: '**', pathMatch: 'full', redirectTo: 'login' },


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
exports: [RouterModule]
})

export class AppRoutingModule { }
